#!/bin/bash
#TaskManager:
ssh-keyscan prod-XXXX >> ~/.ssh/known_hosts
#GitLab:
ssh-keyscan gitlab-ce-XXXX >> ~/.ssh/known_hosts

#Check Uptime on server:
ansible -i inventory.ini prod -m shell -a "uptime"

#Check Bash version in the server:
ansible -i inventory.ini all -m command -a "bash --version"

#Install Nginx Server in Prod Server:
ansible-playbook -i inventory.ini playbook.yml


